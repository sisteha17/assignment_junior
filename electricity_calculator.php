<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Electricity Calculator</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
        }
        .container {
            max-width: 600px;
            margin: 50px auto;
            background: #fff;
            padding: 30px;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }
        h1 {
            text-align: center;
            color: #333;
        }
        form {
            display: flex;
            flex-direction: column;
        }
        .form-group {
            margin-bottom: 15px;
        }
        label {
            margin-bottom: 5px;
            color: #555;
        }
        input[type="number"] {
            padding: 10px;
            border: 1px solid #ccc;
            border-radius: 4px;
            width: 100%;
        }
        button {
            padding: 10px 15px;
            background-color: #007BFF;
            border: none;
            border-radius: 4px;
            color: white;
            font-size: 16px;
            cursor: pointer;
            margin-top: 10px;
        }
        button:hover {
            background-color: #0056b3;
        }
        .results {
            margin-top: 20px;
            padding: 20px;
            background: #e9e9e9;
            border-radius: 8px;
        }
        .results p {
            margin: 0 0 10px;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Electricity Calculator</h1>
        <form method="post" action="">
            <div class="form-group">
                <label for="voltage">Voltage (V)</label>
                <input type="number" id="voltage" name="voltage" required>
            </div>
            <div class="form-group">
                <label for="current">Current (A)</label>
                <input type="number" id="current" name="current" required>
            </div>
            <div class="form-group">
                <label for="hours">Hours</label>
                <input type="number" id="hours" name="hours" required>
            </div>
            <div class="form-group">
                <label for="rate">Current Rate (cents/kWh)</label>
                <input type="number" id="rate" name="rate" required>
            </div>
            <button type="submit">Calculate</button>
        </form>

        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $voltage = $_POST['voltage'];
            $current = $_POST['current'];
            $hours = $_POST['hours'];
            $rate = $_POST['rate'];

            function calculate_power($voltage, $current) {
                return $voltage * $current;
            }

            function calculate_energy($power, $hours) {
                return ($power * $hours) / 1000;
            }

            function calculate_total_charge($energy, $rate) {
                return $energy * ($rate / 100);
            }

            $power = calculate_power($voltage, $current);
            $energy = calculate_energy($power, $hours);
            $total_charge = calculate_total_charge($energy, $rate);

            echo "<div class='results'>";
            echo "<h2>Results</h2>";
            echo "<p>Power (Wh): " . $power . "</p>";
            echo "<p>Energy (kWh): " . $energy . "</p>";
            echo "<p>Total Charge: RM" . $total_charge . "</p>";
            echo "</div>";
        }
        ?>
    </div>
</body>
</html>
