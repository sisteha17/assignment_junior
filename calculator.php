<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Electricity Cost Calculator</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <div class="container mt-5">
        <h1>Electricity Cost Calculator</h1>
        <form method="POST" action="">
            <div class="mb-3">
                <label for="voltage" class="form-label">Voltage (V)</label>
                <input type="number" class="form-control" id="voltage" name="voltage" value="19" required>
            </div>
            <div class="mb-3">
                <label for="current" class="form-label">Current (A)</label>
                <input type="number" class="form-control" id="current" name="current" value="3.24" required>
            </div>
            <div class="mb-3">
                <label for="rate" class="form-label">Current Rate (sen/kWh)</label>
                <input type="number" class="form-control" id="rate" name="rate" value="21.80" required>
            </div>
            <button type="submit" class="btn btn-primary">Calculate</button>
        </form>

        <?php
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $voltage = floatval($_POST['voltage']);
            $current = floatval($_POST['current']);
            $rate = floatval($_POST['rate']) / 100; // converting sen to RM

            $power = $voltage * $current / 1000; // Power in kW

            echo "<h2 class='mt-4'>Results</h2>";
            echo "<p>Power: " . number_format($power, 5) . " kW</p>";
            echo "<table class='table table-bordered mt-3'>";
            echo "<thead><tr><th>Hour</th><th>Energy (kWh)</th><th>Total Cost (RM)</th></tr></thead>";
            echo "<tbody>";

            for ($hour = 1; $hour <= 24; $hour++) {
                $energy = $power * $hour; // Energy in kWh
                $totalCost = $energy * $rate; // Total cost in RM
                echo "<tr><td>$hour</td><td>" . number_format($energy, 5) . "</td><td>" . number_format($totalCost, 2) . "</td></tr>";
            }

            echo "</tbody></table>";
        }
        ?>
    </div>
</body>
</html>
