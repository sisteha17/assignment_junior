# assignment_junior



You will have 2 days to complete the test. You may use whatever resources you like as long as you are following the instructions below:

Write a programme in **PHP plain (vanila)** to calculate power, energy, and the total charge based on the current rate.
You can use any **CSS framework (for example, Foundation or Bootstrap 4)**. 
You have to **upload your source code to Git**.

The electricity consumption charge is easy to understand and calculate. It is measured in kWh (kilowatt-hours). The rate of an electricity bill varies. Suppose we have the following conditions for calculating the rates of electricity:

**Power (Wh) = Voltage (V) * Current  (A)**

**Energy (kWh) = Power * Hour * 1000 ;**

**Total = Energy(kWh) * (current rate/100);**


You must write a function that can calculate electricity rates per hour and per day based on user input of voltage, current (A), and current rate. You may refer to the example in document calculator.pdf.
https://gitlab.com/sukor-muhammad/assignment_junior/-/blob/main/calculater.pdf


https://www.tnb.com.my/residential/pricing-tariffs


